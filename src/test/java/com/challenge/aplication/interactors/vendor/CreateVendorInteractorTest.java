package com.challenge.aplication.interactors.vendor;

import com.challenge.aplication.impl.vendor.request.VendorDto;
import com.challenge.domain.entities.Vendor;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@MicronautTest
class CreateVendorInteractorTest {

    @Inject
    CreateVendorInteractor createVendorInteractor;

    @Test
    void createVendor_success() {
        var mVendorDto = new VendorDto("125440121", "ciclano da silva");

        var vendor = this.createVendorInteractor.createVendor(mVendorDto);

        assertNotNull(vendor);
        assertEquals(mVendorDto.getName(), vendor.getName());
        assertEquals(mVendorDto.getRegistry(), vendor.getRegistry());
    }

}
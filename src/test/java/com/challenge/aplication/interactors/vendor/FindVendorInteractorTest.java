package com.challenge.aplication.interactors.vendor;

import com.challenge.aplication.exceptions.NotFoundException;
import com.challenge.aplication.impl.vendor.request.VendorDto;
import com.challenge.domain.entities.Vendor;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.*;

@MicronautTest
class FindVendorInteractorTest {

    @Inject
    FindVendorInteractor findVendorInteractor;

    @Inject
    EntityManager entityManager;

    @Test
    void find_success() {
        var vendorDto = new VendorDto("125440121", "ciclano da silva");
        entityManager.persist(vendorDto.toDomain());

        var vendor = findVendorInteractor.find("125440121");

        assertNotNull(vendor);
        assertEquals(vendor.getRegistry(), vendorDto.getRegistry());
        assertEquals(vendor.getName(), vendorDto.getName());
    }
    @Test
    void find_not_found() {
        assertThrows(NotFoundException.class, () -> {
            findVendorInteractor.find("125440121");
        });
    }

}
package com.challenge.aplication.interactors.vendor;

import com.challenge.aplication.exceptions.NotFoundException;
import com.challenge.aplication.impl.vendor.request.UpdateVendorRequest;
import com.challenge.aplication.impl.vendor.request.VendorDto;
import com.challenge.domain.entities.Vendor;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.*;

@MicronautTest
class UpdateVendorInteractorTest {

    @Inject
    UpdateVendorInteractor updateVendorInteractor;

    @Inject
    EntityManager entityManager;

    @Test
    void updateVendor_success() {
        entityManager.persist(new VendorDto("125440121", "ciclano da silva").toDomain());

        var updateVendorReq = new UpdateVendorRequest("ciclano da silva");
        var mVendor = this.updateVendorInteractor.updateVendor("125440121", updateVendorReq);

        assertNotNull(mVendor);
        assertEquals(updateVendorReq.getName(), mVendor.getName());
    }

    @Test
    void updateVendor_not_found() {
        assertThrows(NotFoundException.class, () -> {
            var updateVendorReq = new UpdateVendorRequest("test fail");
            this.updateVendorInteractor.updateVendor("123456789", updateVendorReq);
        });
    }
}
package com.challenge.aplication.interactors.vendor;

import com.challenge.aplication.impl.vendor.request.VendorDto;
import com.challenge.domain.entities.Vendor;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.*;

@MicronautTest
class DeleteVendorInteractorTest {

    @Inject
    DeleteVendorInteractor deleteVendorInteractor;

    @Inject
    EntityManager entityManager;

    @Test
    void deleteVendor_success() {
        var vendor = new VendorDto("125440121", "ciclano da silva").toDomain();
        entityManager.persist(vendor);

        assertDoesNotThrow(() -> {
            deleteVendorInteractor.deleteVendor(vendor.getRegistry());
        });
    }
}
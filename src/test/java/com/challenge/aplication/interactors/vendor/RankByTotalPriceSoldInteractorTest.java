package com.challenge.aplication.interactors.vendor;

import com.challenge.domain.entities.Vendor;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@MicronautTest
class RankByTotalPriceSoldInteractorTest {

    @Inject
    RankByTotalPriceSoldInteractor rankByTotalPriceSoldInteractor;

    @Test
    void rankByTotalPriceSold_success() {
        var vendors = rankByTotalPriceSoldInteractor.rankByTotalPriceSold(Pageable.from(1, 15));
        assertNotNull(vendors);
    }
}
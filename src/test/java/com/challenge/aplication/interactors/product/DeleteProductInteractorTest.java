package com.challenge.aplication.interactors.product;

import com.challenge.aplication.impl.product.request.ProductDto;
import com.challenge.aplication.impl.product.request.UpdateProductRequest;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

@MicronautTest
class DeleteProductInteractorTest {

    @Inject
    DeleteProductInteractor deleteProductInteractor;

    @Inject
    EntityManager entityManager;

    @Test
    void deleteProduct_success() {

        var product = new ProductDto(1L,"product 1", new BigDecimal("100.00")).toDomain();
        entityManager.persist(product);

        assertDoesNotThrow(() -> {
            deleteProductInteractor.deleteProduct(product.getId());
        });
    }
}
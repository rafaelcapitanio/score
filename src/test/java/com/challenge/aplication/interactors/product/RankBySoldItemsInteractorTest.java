package com.challenge.aplication.interactors.product;

import com.challenge.domain.entities.Product;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@MicronautTest
class RankBySoldItemsInteractorTest {

    @Inject
    RankBySoldItemsInteractor rankBySoldItemsInteractor;

    @Test
    void getRankBySoldItems_success() {
        var rankBySoldItems = this.rankBySoldItemsInteractor.getRankBySoldItems(Pageable.from(1, 15));

        assertNotNull(rankBySoldItems);
    }
}
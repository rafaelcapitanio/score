package com.challenge.aplication.interactors.product;

import com.challenge.aplication.impl.product.request.ProductDto;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

@MicronautTest
class CreateProductInteractorTest {

    @Inject
    CreateProductInteractor createProductInteractor;

    @Test
    void createProduct_success() {
        var mProductDto = new ProductDto(1L,"productName", new BigDecimal("100.00"));

        var product = this.createProductInteractor.createProduct(mProductDto);

        assertNotNull(product);
        assertEquals(mProductDto.getProductName(), product.getProductName());
    }
}
package com.challenge.aplication.interactors.product;

import com.challenge.aplication.exceptions.NotFoundException;
import com.challenge.aplication.impl.product.request.ProductDto;
import com.challenge.aplication.impl.product.request.UpdateProductRequest;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

@MicronautTest
class UpdateProductInteractorTest {

    @Inject
    UpdateProductInteractor updateProductInteractor;

    @Inject
    EntityManager entityManager;

    @Test
    void updateProduct_success() {
        var productDto = new ProductDto(1L,"product 1", new BigDecimal("100.00"));

        entityManager.persist(productDto.toDomain());

        var product = this.updateProductInteractor.updateProduct(1L, new UpdateProductRequest("product 1", new BigDecimal("300.00")));

        assertNotNull(product);
        assertEquals(product.getProductName(), productDto.getProductName());
        assertEquals(1L, product.getId());
    }

    @Test
    void updateVendor_not_found() {
        assertThrows(NotFoundException.class, () -> {
            var productDto = new UpdateProductRequest("product 1", new BigDecimal("100.00"));
            var product = this.updateProductInteractor.updateProduct(1L, productDto);
        });

    }
}
package com.challenge.aplication.interactors.order;

import com.challenge.aplication.exceptions.NotFoundException;
import com.challenge.aplication.impl.order.request.ItemDto;
import com.challenge.aplication.impl.order.request.OrderDto;
import com.challenge.aplication.impl.vendor.request.VendorDto;
import com.challenge.domain.entities.Order;
import com.challenge.domain.entities.Product;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@MicronautTest
class CreateOrderInteractorTest {

    @Inject
    CreateOrderInteractor createOrderInteractor;

    @Inject
    EntityManager entityManager;

    @Test
    void create_success() {
        OrderDto mOrder = mockOrder();

        Order order = this.createOrderInteractor.create(mOrder);

        assertNotNull(order);
    }

    @Test
    void create_vendor_not_found() {

        assertThrows(NotFoundException.class, () -> {
            OrderDto mOrder = new OrderDto(
                    new VendorDto("0000", "not found"),
                    Arrays.asList(new ItemDto(1L,"product 1", 1)),
                    new BigDecimal("100.00"));

            Order order = this.createOrderInteractor.create(mOrder);
        });
    }

    @Test
    void create_product_not_found() {

        assertThrows(NotFoundException.class, () -> {
            OrderDto mOrder = new OrderDto(
                    new VendorDto("125440121", "ciclano da silva"),
                    Arrays.asList(new ItemDto(1L,"product not found", 1)),
                    new BigDecimal("100.00"));

            Order order = this.createOrderInteractor.create(mOrder);
        });
    }

    private OrderDto mockOrder () {

        VendorDto mVendorDto = new VendorDto("125440121", "ciclano da silva");

        entityManager.persist(mVendorDto.toDomain());
        entityManager.persist(new Product(1L,"product 1", new BigDecimal("100.00")));
        entityManager.persist(new Product(2L,"product 2", new BigDecimal("150.00")));

        ItemDto item1 = new ItemDto(1L,"product 1", 1);
        ItemDto item2 = new ItemDto(2L,"product 2", 2);

        List<ItemDto> listProducts = Arrays.asList(item1, item2);

        return new OrderDto(mVendorDto, listProducts, new BigDecimal("400.00"));
    }
}
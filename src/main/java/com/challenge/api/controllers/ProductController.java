package com.challenge.api.controllers;

import com.challenge.aplication.impl.product.request.ProductDto;
import com.challenge.aplication.impl.product.request.UpdateProductRequest;
import com.challenge.aplication.interactors.product.CreateProductInteractor;
import com.challenge.aplication.interactors.product.DeleteProductInteractor;
import com.challenge.aplication.interactors.product.RankBySoldItemsInteractor;
import com.challenge.aplication.interactors.product.UpdateProductInteractor;
import com.challenge.domain.entities.Product;
import io.micronaut.data.model.Pageable;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import jakarta.inject.Inject;

@Controller("/v1/product")
public class ProductController {

    private CreateProductInteractor createProductInteractor;
    private DeleteProductInteractor deleteProductInteractor;
    private UpdateProductInteractor updateProductInteractor;
    private RankBySoldItemsInteractor rankBySoldItemsInteractor;


    @Inject
    public ProductController(
            CreateProductInteractor createProductInteractor,
            DeleteProductInteractor deleteProductInteractor,
            UpdateProductInteractor updateProductInteractor,
            RankBySoldItemsInteractor rankBySoldItemsInteractor)
    {
        this.createProductInteractor = createProductInteractor;
        this.deleteProductInteractor = deleteProductInteractor;
        this.updateProductInteractor = updateProductInteractor;
        this.rankBySoldItemsInteractor = rankBySoldItemsInteractor;
    }

    @Post
    public HttpResponse create (@Body ProductDto productDto) {
        Product product = this.createProductInteractor.createProduct(productDto);
        return HttpResponse.created(product);
    }

    @Put(value = "/{productId}" , produces = MediaType.APPLICATION_JSON)
    public HttpResponse update (@PathVariable Long productId, @Body UpdateProductRequest productDto) {
        Product product = this.updateProductInteractor.updateProduct(productId, productDto);
        return HttpResponse.ok(product);
    }

    @Delete(value = "/{productId}")
    public HttpResponse delete (@PathVariable Long productId) {
        this.deleteProductInteractor.deleteProduct(productId);
        return HttpResponse.noContent();
    }

    @Get(value = "/rankbysolditems")
    public HttpResponse getRankedProductBySoldItems(Pageable pageable){
        var page = this.rankBySoldItemsInteractor.getRankBySoldItems(pageable);
        return HttpResponse.ok(page);

    }

}

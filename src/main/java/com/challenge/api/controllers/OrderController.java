package com.challenge.api.controllers;

import com.challenge.aplication.impl.order.request.OrderDto;
import com.challenge.aplication.interactors.order.CreateOrderInteractor;
import com.challenge.domain.services.OrderService;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;
import jakarta.inject.Inject;

@Controller("/v1/order")
public class OrderController {

    private CreateOrderInteractor createOrderInteractor;

    @Inject
    public OrderController(CreateOrderInteractor createOrderInteractor){
        this.createOrderInteractor = createOrderInteractor;
    }

    @Post
    public HttpResponse create ( @Body OrderDto orderDto) {
        var order = createOrderInteractor.create(orderDto);
        return HttpResponse.created(order);
    }
    
}

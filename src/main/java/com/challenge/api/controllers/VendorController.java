package com.challenge.api.controllers;

import com.challenge.aplication.impl.vendor.request.UpdateVendorRequest;
import com.challenge.aplication.impl.vendor.request.VendorDto;
import com.challenge.aplication.interactors.vendor.*;
import com.challenge.domain.entities.Vendor;
import io.micronaut.data.model.Pageable;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;

@Controller("/v1/vendor")
public class VendorController {

    private CreateVendorInteractor createVendorInteractor;
    private DeleteVendorInteractor deleteVendorInteractor;
    private FindVendorInteractor findVendorInteractor;
    private UpdateVendorInteractor updateVendorInteractor;
    private RankByOrdersSoldInteractor rankByOrdersSoldInteractor;
    private RankByTotalPriceSoldInteractor rankByTotalPriceSoldInteractor;

    public VendorController(
            CreateVendorInteractor createVendorInteractor,
            DeleteVendorInteractor deleteVendorInteractor,
            FindVendorInteractor findVendorInteractor,
            UpdateVendorInteractor updateVendorInteractor,
            RankByOrdersSoldInteractor rankByOrdersSoldInteractor,
            RankByTotalPriceSoldInteractor rankByTotalPriceSoldInteractor)
    {
        this.createVendorInteractor = createVendorInteractor;
        this.deleteVendorInteractor = deleteVendorInteractor;
        this.findVendorInteractor = findVendorInteractor;
        this.updateVendorInteractor = updateVendorInteractor;
        this.rankByOrdersSoldInteractor = rankByOrdersSoldInteractor;
        this.rankByTotalPriceSoldInteractor = rankByTotalPriceSoldInteractor;
    }

    @Post
    public HttpResponse create (@Body VendorDto vendorDto) {
        Vendor vendor = createVendorInteractor.createVendor(vendorDto);
        return HttpResponse.created(vendor);
    }

    @Put(value = "/{registry}" , produces = MediaType.APPLICATION_JSON)
    public HttpResponse update (@PathVariable String registry,@Body UpdateVendorRequest request){
        Vendor vendor = updateVendorInteractor.updateVendor(registry, request);
        return HttpResponse.ok(vendor);
    }

    @Delete(value = "/{registry}")
    public void delete (@PathVariable String registry) {
        deleteVendorInteractor.deleteVendor(registry);
        HttpResponse.noContent();
    }

    @Get(value = "/{registry}", produces = MediaType.APPLICATION_JSON)
    public HttpResponse getVendorById (@PathVariable String registry) {
        Vendor vendor = findVendorInteractor.find(registry);
        return HttpResponse.ok(vendor);
    }

    @Get(value = "/rankbytotalprice")
    public HttpResponse getRankByTotalPriceSold (Pageable pageable) {
        var page = rankByTotalPriceSoldInteractor.rankByTotalPriceSold(pageable);
        return HttpResponse.ok(page);
    }

    @Get(value = "/rankbysold")
    public HttpResponse getRankBySold (Pageable pageable) {
        var page = rankByOrdersSoldInteractor.rankByOrdersSold(pageable);
        return HttpResponse.ok(page);
    }
    
}

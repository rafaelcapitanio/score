package com.challenge.infrastructure.service;

import com.challenge.domain.entities.Product;
import com.challenge.domain.services.ProductService;
import com.challenge.infrastructure.repositories.ProductRepository;

import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.data.model.Sort;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import java.util.List;
import java.util.Optional;

@Named
public class ProductServiceImpl implements ProductService {

    @Inject
    private ProductRepository repository;

    @Override
    public void createProduct(Product product) {
        this.repository.saveAndFlush(product);
    }

    @Override
    public void updateProduct(Product product) {
        this.repository.update(product);
    }

    @Override
    public void deleteProduct(Long id) {
        this.repository.deleteById(id);
    }

    @Override
    public Optional<Product> findProductById(Long id) {
       return this.repository.findById(id);
    }

    @Override
    public Page<Product> rankBySoldItems(Pageable pageable) {
        return this.repository.findAllProductWithSoldQuantity(pageable);
    }

}

package com.challenge.infrastructure.service;

import com.challenge.domain.entities.Order;
import com.challenge.domain.entities.Product;
import com.challenge.domain.entities.Vendor;
import com.challenge.domain.services.OrderService;
import com.challenge.infrastructure.repositories.OrderRepository;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import java.util.List;

@Named
public class OrderServiceImpl implements OrderService {

    @Inject
    private OrderRepository orderRepository;

    @Override
    public Order createOrder(Order order) {
        return orderRepository.saveAndFlush(order);
    }

    public Order commitOrder(Order order) {
        return orderRepository.update(order);
    }
    
}
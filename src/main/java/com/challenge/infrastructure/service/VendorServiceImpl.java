package com.challenge.infrastructure.service;

import java.util.Optional;

import com.challenge.domain.entities.Vendor;
import com.challenge.domain.services.VendorService;
import com.challenge.infrastructure.repositories.VendorRepository;

import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.data.model.Sort;
import jakarta.inject.Inject;
import jakarta.inject.Named;

@Named
public class VendorServiceImpl implements VendorService {

    @Inject
    private VendorRepository repository;

    @Override
    public void createVendor(Vendor vendor) {
        this.repository.saveAndFlush(vendor);
    }

    @Override
    public Vendor updateVendor(Vendor vendor) {
        return this.repository.update(vendor);
    }

    @Override
    public void deleteVendor(String registry) {
        this.repository.findById(registry);
    }

    @Override
    public Optional<Vendor> findVendor(String registry) {
        return this.repository.findById(registry);
    }

    @Override
    public Page<Vendor> rankByTotalPriceSold(Pageable pageable) {
        return this.repository.rankVendorByOrdersSubtotal(pageable);
    }

    @Override
    public Page<Vendor> rankByOrdersSold(Pageable pageable) {
        return this.repository.rankVendorByOrderQuantity(pageable);
    }


}

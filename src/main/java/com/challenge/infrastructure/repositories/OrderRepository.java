package com.challenge.infrastructure.repositories;

import com.challenge.domain.entities.Order;

import io.micronaut.data.annotation.Repository;
import io.micronaut.data.jpa.repository.JpaRepository;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long>{
    
}

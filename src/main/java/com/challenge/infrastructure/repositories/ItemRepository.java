package com.challenge.infrastructure.repositories;


import com.challenge.domain.entities.Item;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.jpa.repository.JpaRepository;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {

}

package com.challenge.infrastructure.repositories;

import com.challenge.domain.entities.Vendor;

import io.micronaut.data.annotation.Query;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.jpa.repository.JpaRepository;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;

@Repository
public interface VendorRepository extends JpaRepository<Vendor, String>{

    Page<Vendor> findAll(Pageable pageable);

    @Query(value = "SELECT * ,(SELECT COUNT(*) FROM orders o WHERE o.vendor_id = v.registry) AS ordersQuantity FROM vendor v ORDER BY ordersQuantity DESC",
            countQuery = "SELECT count(*) FROM vendor",
            nativeQuery = true)
    Page<Vendor> rankVendorByOrderQuantity(Pageable pageable);

    @Query(value = "SELECT v.registry, v.vendor_name, (SELECT SUM(o.total_price) FROM orders o WHERE o.vendor_id = v.registry) AS ordersSubtotal FROM vendor v ORDER BY ordersSubtotal DESC",
            countQuery = "SELECT count(*) FROM vendor",
            nativeQuery = true)
    Page<Vendor> rankVendorByOrdersSubtotal(Pageable pageable);



}

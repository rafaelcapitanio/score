package com.challenge.infrastructure.repositories;

import com.challenge.domain.entities.Product;

import io.micronaut.data.annotation.Query;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.jpa.repository.JpaRepository;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;

import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long>{

    @Query(value = "SELECT *, (SELECT SUM(i.quantity) FROM items i WHERE i.product_id = p.id) AS soldQuantity FROM products p ORDER BY soldQuantity DESC",
            countQuery = "SELECT count(*) FROM products",
            nativeQuery = true)
    Page<Product> findAllProductWithSoldQuantity(Pageable pageable);


}

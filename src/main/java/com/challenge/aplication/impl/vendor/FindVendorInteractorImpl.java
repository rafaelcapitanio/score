package com.challenge.aplication.impl.vendor;

import com.challenge.aplication.exceptions.NotFoundException;
import com.challenge.aplication.interactors.vendor.FindVendorInteractor;
import com.challenge.aplication.message.Messages;
import com.challenge.domain.entities.Vendor;
import com.challenge.domain.services.VendorService;
import jakarta.inject.Inject;

import jakarta.inject.Named;
import java.util.Optional;

@Named
public class FindVendorInteractorImpl implements FindVendorInteractor {

    @Inject
    private VendorService vendorService;

    @Override
    public Vendor find(String registry) {

        var findVendor = vendorService.findVendor(registry);

        if (findVendor.isPresent()) {
            return findVendor.get();
        }

        throw new NotFoundException(Messages.VENDOR_NOT_FOUND);
    }

}

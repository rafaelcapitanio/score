package com.challenge.aplication.impl.vendor;

import com.challenge.aplication.exceptions.ConflictException;

import com.challenge.aplication.impl.vendor.request.VendorDto;
import com.challenge.aplication.interactors.vendor.CreateVendorInteractor;
import com.challenge.aplication.message.Messages;
import com.challenge.domain.entities.Vendor;
import com.challenge.domain.services.VendorService;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import javax.transaction.Transactional;

@Named
public class CreateVendorInteractorImpl implements CreateVendorInteractor {

    @Inject
    private VendorService vendorService;

    @Override
    @Transactional
    public Vendor createVendor(VendorDto vendor) {

        var domain = vendor.toDomain();

        var findVendor = vendorService.findVendor(vendor.getRegistry());

        findVendor.ifPresentOrElse(vend -> {
            throw new ConflictException(Messages.VENDOR_ALREADY_EXISTS);
        }, () -> {
            this.vendorService.createVendor(domain);
        });

        return domain;
    }

}

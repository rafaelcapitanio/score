package com.challenge.aplication.impl.vendor;

import com.challenge.aplication.interactors.vendor.RankByOrdersSoldInteractor;
import com.challenge.domain.entities.Vendor;
import com.challenge.domain.services.VendorService;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import jakarta.inject.Inject;

public class RankByOrdersSoldInteractorImpl implements RankByOrdersSoldInteractor {

    @Inject
    private VendorService vendorService;

    @Override
    public Page<Vendor> rankByOrdersSold(Pageable pageable) {
        return this.vendorService.rankByOrdersSold(pageable);
    }
}

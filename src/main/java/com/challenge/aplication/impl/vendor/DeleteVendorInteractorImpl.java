package com.challenge.aplication.impl.vendor;

import com.challenge.aplication.interactors.vendor.DeleteVendorInteractor;
import com.challenge.domain.services.VendorService;

import jakarta.inject.Inject;
import jakarta.inject.Named;

@Named
public class DeleteVendorInteractorImpl implements DeleteVendorInteractor {

    @Inject
    private VendorService vendorService;

    @Override
    public void deleteVendor(String registry) {
        vendorService.deleteVendor(registry);
    }

}

package com.challenge.aplication.impl.vendor;

import com.challenge.aplication.exceptions.NotFoundException;
import com.challenge.aplication.impl.vendor.request.UpdateVendorRequest;
import com.challenge.aplication.interactors.vendor.UpdateVendorInteractor;
import com.challenge.aplication.message.Messages;
import com.challenge.domain.entities.Vendor;
import com.challenge.domain.services.VendorService;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import java.util.Optional;

@Named
public class UpdateVendorInteractorImpl implements UpdateVendorInteractor {

    @Inject
    private VendorService vendorService;

    @Override
    public Vendor updateVendor(String registry, UpdateVendorRequest updateRequest) {
        var findVendor = vendorService.findVendor(registry);

        findVendor.ifPresentOrElse(vendor -> {
            vendor.setName(updateRequest.getName());
            vendorService.updateVendor(vendor);
        }, () -> {
            throw new NotFoundException(Messages.VENDOR_NOT_FOUND);
        });

        return findVendor.get();
    }

}

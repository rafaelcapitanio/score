package com.challenge.aplication.impl.vendor.request;

import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Introspected
public class UpdateVendorRequest {

    @NotNull @Valid
    private String name;
}

package com.challenge.aplication.impl.vendor;

import com.challenge.aplication.interactors.vendor.RankByTotalPriceSoldInteractor;
import com.challenge.domain.entities.Vendor;
import com.challenge.domain.services.VendorService;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import jakarta.inject.Inject;

public class RankByTotalPriceSoldInteractorImpl implements RankByTotalPriceSoldInteractor {

    @Inject
    private VendorService vendorService;

    @Override
    public Page<Vendor> rankByTotalPriceSold(Pageable pageable) {
        return this.vendorService.rankByTotalPriceSold(pageable);
    }
}

package com.challenge.aplication.impl.vendor.request;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.challenge.domain.entities.Vendor;

import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Introspected
public class VendorDto {

    @NotNull @Valid
    private String registry;

    @NotBlank @Valid
    private String name;

    public Vendor toDomain() {
        return new Vendor(this.registry, this.name);
    }
    
}

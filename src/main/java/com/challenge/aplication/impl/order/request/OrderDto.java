package com.challenge.aplication.impl.order.request;

import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

import com.challenge.aplication.impl.vendor.request.VendorDto;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Introspected
public class OrderDto {

    @NotNull @Valid
    private VendorDto vendor;

    @NotNull @Valid
    private List<ItemDto> items;

    @Positive @Valid
    private BigDecimal totalPrice; 
    
}

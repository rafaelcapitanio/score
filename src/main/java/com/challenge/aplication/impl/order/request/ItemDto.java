package com.challenge.aplication.impl.order.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ItemDto {

    @PositiveOrZero
    private Long productId;

    @NotBlank
    private String productName;

    @Positive
    private Integer quantity;
}

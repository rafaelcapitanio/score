package com.challenge.aplication.impl.order;

import com.challenge.aplication.exceptions.NotFoundException;
import com.challenge.aplication.exceptions.OutOfDataException;
import com.challenge.aplication.impl.order.request.OrderDto;
import com.challenge.aplication.interactors.order.CreateOrderInteractor;
import com.challenge.aplication.message.Messages;
import com.challenge.domain.entities.Item;
import com.challenge.domain.entities.Order;
import com.challenge.domain.services.OrderService;
import com.challenge.domain.services.ProductService;
import com.challenge.domain.services.VendorService;
import com.challenge.infrastructure.repositories.ItemRepository;
import jakarta.inject.Inject;
import jakarta.inject.Named;

@Named
public class CreateOrderInteractorImpl implements CreateOrderInteractor {

    private OrderService orderService;
    private ProductService productService;
    private VendorService vendorService;
    private ItemRepository itemRepository;

    @Inject
    public CreateOrderInteractorImpl (
            OrderService orderService,
            ProductService productService,
            VendorService vendorService,
            ItemRepository itemRepository)
    {
        this.orderService = orderService;
        this.productService = productService;
        this.vendorService = vendorService;
        this.itemRepository = itemRepository;
    }

    @Override
    public Order create(OrderDto orderDto) {
        var order = orderService.createOrder(new Order());

        var vendorDto = orderDto.getVendor();
        var items = orderDto.getItems();

        var opVendor = vendorService.findVendor(vendorDto.getRegistry());

        opVendor.ifPresentOrElse(vendor -> {
            order.setVendor(vendor);
        }, () -> {
            throw new NotFoundException(Messages.VENDOR_NOT_FOUND);
        });

        items.stream().forEach(itemDto -> {
           var opProduct = productService.findProductById(itemDto.getProductId());

           opProduct.ifPresentOrElse(product -> {
               Item item = itemRepository.saveAndFlush(new Item(product, itemDto.getQuantity(), order));
               order.addItem(item);
           }, () -> {
               throw new NotFoundException(Messages.PRODUCT_NOT_FOUND);
           });
        });

        if (orderDto.getTotalPrice().compareTo(order.getTotalPrice()) != 0) {
            throw new OutOfDataException(Messages.PRICE_IS_OUT_OF_DATA);
        }

        order.setTotalPrice(orderDto.getTotalPrice());

        return orderService.commitOrder(order);
    }

}

package com.challenge.aplication.impl.product.request;

import com.challenge.domain.entities.Product;
import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Introspected
public class ProductDto {

    @Valid
    @PositiveOrZero
    private Long id;

    @NotBlank
    @Valid
    private String productName;

    @PositiveOrZero
    @Valid
    private BigDecimal price;

    public Product toDomain(){
        return new Product(this.id, this.productName, this.price);
    }
}

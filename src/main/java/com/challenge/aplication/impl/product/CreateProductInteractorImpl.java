package com.challenge.aplication.impl.product;

import com.challenge.aplication.exceptions.ConflictException;
import com.challenge.aplication.impl.product.request.ProductDto;
import com.challenge.aplication.interactors.product.CreateProductInteractor;
import com.challenge.domain.entities.Product;
import com.challenge.domain.services.ProductService;

import jakarta.inject.Inject;
import jakarta.inject.Named;

@Named
public class CreateProductInteractorImpl implements CreateProductInteractor {

    @Inject
    private ProductService productService;

    @Override
    public Product createProduct(ProductDto product) {

        var domainProduct = product.toDomain();
        var opProduct = productService.findProductById(product.getId());

        opProduct.ifPresentOrElse(prodList -> {
            throw new ConflictException("Product already exists");
        }, () -> {
           productService.createProduct(domainProduct);
        });

        return domainProduct;
    }

}

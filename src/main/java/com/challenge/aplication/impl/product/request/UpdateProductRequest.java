package com.challenge.aplication.impl.product.request;

import java.math.BigDecimal;

import com.challenge.domain.entities.Product;

import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Introspected
public class UpdateProductRequest {

    @NotBlank @Valid
    private String productName;

    @PositiveOrZero @Valid
    private BigDecimal price;



}

package com.challenge.aplication.impl.product;

import com.challenge.aplication.interactors.product.RankBySoldItemsInteractor;
import com.challenge.domain.entities.Product;
import com.challenge.domain.services.ProductService;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import jakarta.inject.Inject;

import java.util.List;

public class RankBySoldItemsInteractorImpl implements RankBySoldItemsInteractor {

    @Inject
    private ProductService productService;

    @Override
    public Page<Product> getRankBySoldItems(Pageable pageable) {
        return this.productService.rankBySoldItems(pageable);
    }
}

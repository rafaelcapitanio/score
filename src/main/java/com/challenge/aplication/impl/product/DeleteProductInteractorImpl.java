package com.challenge.aplication.impl.product;

import com.challenge.aplication.interactors.product.DeleteProductInteractor;
import com.challenge.domain.services.ProductService;
import jakarta.inject.Inject;

import jakarta.inject.Named;

@Named
public class DeleteProductInteractorImpl implements DeleteProductInteractor {

    @Inject
    private ProductService productService;

    @Override
    public void deleteProduct(Long id) {
        productService.deleteProduct(id);
    }

}

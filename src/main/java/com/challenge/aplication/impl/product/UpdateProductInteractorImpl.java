package com.challenge.aplication.impl.product;

import com.challenge.aplication.exceptions.NotFoundException;
import com.challenge.aplication.impl.product.request.UpdateProductRequest;
import com.challenge.aplication.interactors.product.UpdateProductInteractor;
import com.challenge.aplication.message.Messages;
import com.challenge.domain.entities.Product;
import com.challenge.domain.services.ProductService;

import jakarta.inject.Inject;
import jakarta.inject.Named;

@Named
public class UpdateProductInteractorImpl implements UpdateProductInteractor {

    @Inject
    private ProductService productService;

    @Override
    public Product updateProduct(Long id, UpdateProductRequest product) {
        var opProduct = productService.findProductById(id);

        opProduct.ifPresentOrElse(prod -> {
            prod.setPrice(product.getPrice());
            prod.setProductName(product.getProductName());
            productService.updateProduct(prod);
        }, () -> {
            throw new NotFoundException(Messages.PRODUCT_NOT_FOUND);
        });

        return opProduct.get();
    }

}

package com.challenge.aplication.exceptions;

import io.micronaut.http.HttpStatus;
import io.micronaut.http.exceptions.HttpStatusException;

public class ConflictException extends HttpStatusException {

    public ConflictException(String message) {
        super(HttpStatus.CONFLICT, message);
    }

}

package com.challenge.aplication.exceptions;

import io.micronaut.http.HttpStatus;
import io.micronaut.http.exceptions.HttpStatusException;

public class OutOfDataException extends HttpStatusException {

    public OutOfDataException(String message) {
        super(HttpStatus.BAD_REQUEST, message);
    }

}

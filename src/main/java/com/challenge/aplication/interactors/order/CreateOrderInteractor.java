package com.challenge.aplication.interactors.order;

import com.challenge.aplication.impl.order.request.OrderDto;
import com.challenge.domain.entities.Order;

public interface CreateOrderInteractor {

    Order create(OrderDto orderDto);
}

package com.challenge.aplication.interactors.product;

public interface DeleteProductInteractor {

    void deleteProduct(Long id);
    
}

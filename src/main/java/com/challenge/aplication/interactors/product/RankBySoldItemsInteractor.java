package com.challenge.aplication.interactors.product;

import com.challenge.domain.entities.Product;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;

import java.util.List;
import java.util.Optional;

public interface RankBySoldItemsInteractor {

    Page<Product> getRankBySoldItems(Pageable pageable);

}

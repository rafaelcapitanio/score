package com.challenge.aplication.interactors.product;

import com.challenge.aplication.impl.product.request.UpdateProductRequest;
import com.challenge.domain.entities.Product;

public interface UpdateProductInteractor {

    Product updateProduct(Long id, UpdateProductRequest product);
    
}

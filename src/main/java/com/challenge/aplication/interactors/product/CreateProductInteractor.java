package com.challenge.aplication.interactors.product;

import com.challenge.aplication.impl.product.request.ProductDto;
import com.challenge.domain.entities.Product;

public interface CreateProductInteractor {

    Product createProduct(ProductDto product);

}

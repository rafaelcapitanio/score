package com.challenge.aplication.interactors.vendor;

import com.challenge.domain.entities.Vendor;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;

public interface RankByOrdersSoldInteractor {

    Page<Vendor> rankByOrdersSold (Pageable pageable);
}

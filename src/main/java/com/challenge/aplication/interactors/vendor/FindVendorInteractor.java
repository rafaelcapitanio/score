package com.challenge.aplication.interactors.vendor;

import com.challenge.domain.entities.Vendor;

public interface FindVendorInteractor {

    Vendor find(String registry);
    
}

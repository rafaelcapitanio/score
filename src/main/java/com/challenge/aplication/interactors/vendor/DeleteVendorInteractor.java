package com.challenge.aplication.interactors.vendor;

public interface DeleteVendorInteractor {

    void deleteVendor(String registry);
    
}

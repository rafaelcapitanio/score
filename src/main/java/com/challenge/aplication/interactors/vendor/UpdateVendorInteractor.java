package com.challenge.aplication.interactors.vendor;

import com.challenge.aplication.impl.vendor.request.UpdateVendorRequest;
import com.challenge.domain.entities.Vendor;

public interface UpdateVendorInteractor {

    Vendor updateVendor(String registry, UpdateVendorRequest updateRequest);

}

package com.challenge.aplication.interactors.vendor;

import com.challenge.aplication.impl.vendor.request.VendorDto;
import com.challenge.domain.entities.Vendor;

public interface CreateVendorInteractor {

    Vendor createVendor(VendorDto vendor);

}

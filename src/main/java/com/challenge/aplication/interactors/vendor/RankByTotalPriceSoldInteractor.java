package com.challenge.aplication.interactors.vendor;

import com.challenge.domain.entities.Vendor;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;

public interface RankByTotalPriceSoldInteractor {

    Page<Vendor> rankByTotalPriceSold (Pageable pageable);

}

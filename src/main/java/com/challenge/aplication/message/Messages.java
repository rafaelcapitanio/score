package com.challenge.aplication.message;

public class Messages {

    public static final String PRODUCT_NOT_FOUND = "Product does not exists";
    public static final String VENDOR_NOT_FOUND = "Vendor does not exists";
    public static final String PRICE_IS_OUT_OF_DATA = "TotalPrice is out of data, please update the price.";
    public static final String VENDOR_ALREADY_EXISTS = "Vendor already exists";

}

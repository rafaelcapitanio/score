package com.challenge.domain.entities;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "orders")
@NoArgsConstructor
public class Order {

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference
    @JoinColumn(name="vendor_id", referencedColumnName = "registry")
    private Vendor vendor;

    @OneToMany(mappedBy = "order", targetEntity=Item.class,
            cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JsonManagedReference
    private List<Item> items = new ArrayList<>();

    @Column(name = "total_price", nullable = false)
    private BigDecimal totalPrice = BigDecimal.ZERO;

    public Order (Vendor vendor, List<Item> items) {
        this.vendor = vendor;
        this.items = items;

        items.stream().forEach(item -> this.totalPrice.add(item.getSubTotal()));
    }

    public void addItem (Item item) {
        this.totalPrice = this.totalPrice.add(item.getSubTotal());
        this.items.add(item);
    }



}

package com.challenge.domain.entities;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "vendor")
@AllArgsConstructor
@RequiredArgsConstructor
public class Vendor {

    @Id
    @Column(name = "registry", nullable = false)
    private String registry;

    @Column(name = "vendor_name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "vendor", fetch = FetchType.EAGER, targetEntity=Order.class,
            cascade=CascadeType.ALL, orphanRemoval = true)
    @JsonProperty( value = "items", access = JsonProperty.Access.WRITE_ONLY)
    @JsonManagedReference
    private List<Order> orders = new ArrayList<>();

    /**
     * TODO query set these fields
    @Transient
    private BigDecimal ordersSubtotal;

    @Transient
    private Integer ordersQuantity;
    */
    public Vendor (String registry, String name){
        this.registry = registry;
        this.name = name;
    }

}

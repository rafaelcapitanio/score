package com.challenge.domain.services;

import com.challenge.domain.entities.Product;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;

import java.util.Optional;


public interface ProductService {

    void createProduct (Product product);

    void updateProduct (Product product);

    void deleteProduct (Long id);

    Optional<Product> findProductById(Long id);

    Page<Product> rankBySoldItems(Pageable pageable);
    
}

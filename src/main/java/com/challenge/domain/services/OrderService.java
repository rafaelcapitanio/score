package com.challenge.domain.services;

import java.util.List;

import com.challenge.domain.entities.Order;


public interface OrderService {

    Order createOrder (Order order);

    Order commitOrder (Order order);

}

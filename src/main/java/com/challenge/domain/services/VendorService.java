package com.challenge.domain.services;

import java.util.Optional;

import com.challenge.domain.entities.Vendor;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;

public interface VendorService {

    void createVendor (Vendor vendor);

    Vendor updateVendor (Vendor vendor);

    void deleteVendor (String registry);

    Optional<Vendor> findVendor(String registry);

    Page<Vendor> rankByTotalPriceSold (Pageable pageable);

    Page<Vendor> rankByOrdersSold (Pageable pageable);

}

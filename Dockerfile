FROM openjdk:11
VOLUME /tmp
ARG JAVA_OPTS="-Dcom.sun.management.jmxremote -Xmx128m"
ENV JAVA_OPTS=$JAVA_OPTS
COPY target/score-*.jar score.jar
EXPOSE 8080
ENTRYPOINT exec java $JAVA_OPTS -jar score.jar
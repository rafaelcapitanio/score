# SCORE

### Cadastrar Produto
 <details>
    <summary>Click to expand</summary>

POST /v1/product/

    request:
    
    {   
        "id": 2,
        "productName" : "product 2",
        "price" : "150.00"
    }
    
    response:
    
    {
        "id": 2,
        "productName": "product 2",
        "price": 150.00
    }
</details>


### Atualizar Produtos

<details>
    <summary>Click to expand</summary>

PUT /v1/product/{producId}

    request:

    {   
        "productName" : "product 1",
        "price" : "200.00"
    }
    
    response:
    
    {
        "id": 1,
        "productName": "product 1",
        "price": 200.00
    }
</details>

### Deletar Produto

<details>
    <summary>Click to expand</summary>

DELETE /v1/product/{producId}

</details>

### Lista de Produtos mais vendidos

<details>
    <summary>Click to expand</summary>

GET /v1/product/rankbysolditems

    response:
    
    {
      "content": [
          {
              "id": 1,
              "productName": "product 1",
              "price": 100.00
          },
          {
              "id": 2,
              "productName": "product 2",
              "price": 150.00
          }
      ],
      "pageable": {
          "number": 0,
          "sort": {
              "sorted": false
          },
          "size": 30,
          "offset": 0,
          "sorted": false
      },
      "totalSize": 2,
      "totalPages": 1,
      "empty": false,
      "size": 30,
      "offset": 0,
      "numberOfElements": 2,
      "pageNumber": 0
    }
</details>

### Cadastrar Vendedor

<details>
    <summary>Click to expand</summary>

POST /v1/vendor/ 

    request:
    
    {
    "registry" : "123456789",
    "name" : "ciclano da silva"
    }
    
    response:
    
    {
    "registry": "123456789",
    "name": "ciclano da silva"
    }
</details>

### Listar Vendedor

<details>
    <summary>Click to expand</summary>
</details>

### Atualizar Vendedor

<details>
    <summary>Click to expand</summary>

PUT /v1/vendor/123456789

    request:
    
    {
      "name" : "ciclano da silva"
    }
    
    response:
    
    {
      "registry": "123456789",
      "name": "ciclano da silva"
    }
</details>

### Deletar Vendedor

<details>
    <summary>Click to expand</summary>

DELETE /v1/vendor/123456789

</details>

### Lista dos Vendedores por maior número de vendas

<details>
    <summary>Click to expand</summary>

GET /v1/vendor/rankbysold/

</details>

### Lista dos Vendedores por valor de vendas

<details>
    <summary>Click to expand</summary>

GET /v1/vendor/rankbytotalprice

</details>


### Cadastrar de venda

<details>
    <summary>Click to expand</summary>

POST /v1/order/

    request:
    
    {
      "vendor": {
          "registry" : "123456789",
          "name" : "ciclano da silva"
      },
      "items": [
          {
              "productId" : 1,
              "productName" : "product 1",
              "quantity": 2
          },
          {
              "productId" : 2,
              "productName" : "product 2",
              "quantity": 1
          }
      ],
      "totalPrice" : 350
    }
    
    response:
    
    {
      "id": 8,
      "items": [
          {
              "id": 15,
              "quantity": 2,
              "subTotal": 200.00
          },
          {
              "id": 16,
              "quantity": 1,
              "subTotal": 150.00
          }
      ],
      "totalPrice": 350
    }

</details>


## Requisitos

### Obrigatórios

- [x] Cadastrar, alterar, deletar e buscar vendedores
    - Um vendedor é composto por um nome e matrícula
    - Apenas a busca por matrícula é necessária
- [x] Cadastrar, alterar, deletar produto
    - Um produto é composto por nome e preço
    - Não é necessária busca por produto
- [x] Cadastrar de venda
    - Uma venda é definida por um vendedor, 1 ou mais itens e o valor total
- [x] Lista dos Vendedores por maior número de vendas
- [x] Lista dos Vendedores por valor de vendas
- [x] Lista de Produtos mais vendidos


## Architecture example:

![](architecture-suggest.png)